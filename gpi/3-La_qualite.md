# Que'est-ce que la qualité ?
La notion de qualité a évolué en informatique depuis 1951 :

- L'aptitude à l'usage (1951)
- La conformité aux spécifications (1979)
- L'aptitude à satisfaure le client (1984)
- L'anticipation des besoins (2000)
- La citoyenneté (2010 ?)

## Les 4 composantes de la qualité

- Qualité de définition
- Qualité de conception
- Qualité de réalisation
- Qualité de service

## Les enjeux de la qualité
### Enjeu pour le client
- La satisfaction
    + Il oublie le prix qu'il a payé
    + Il oublie le temps pendant lequel il a impatiemment attendu car prix et livraison n'ont lieu qu'une seule fois
    + Il se souvient des services qu'elle lui a rendus... ou refusés, car __l'usage est de tous les jours__.
- La fidélisation qui rapporte plus que la conquête

### Enjeu pour le collaborateur
- Implication : Toute personne peut contribuer à l'amélioration de son travail
- Un mangement mobilisateur

### Pour l'entreprise
- Du savoir faire et des économies, car c'est la non-qualité qui coûte cher :
    + 3.9% du CA
    + 10.6% de sa VA
    + 2/3 de son bénéfice brut

## La qualité du développement informatique
Deux angles pour un logiciel :

- Les fonctions qu'il réalise
- Les caractéristiques de l'utilisation qui comprend :
    + Ergonomie
    + Conditions d'exploitation
    + Correction des erreurs résiduelles
    + Évolutions fonctionnelles

## L'évaluation sur norme ISO 9126
### 6 caractéristiques
- Capacité fonctionnelle
- Fiabilité
- Facilité d'utilisation
- Efficacité (rendement)
- Capacité à être maintenu
- Portabilité

### 21 sous-caractéristiques
#### Capacité fonctionnelle
- Aptitude : Les fonctions sont celles qui satisfont aux besoins exprimés et implicites pour des tâches données.
- Exactitude : La fourniture des résultats ou d'effets justes ou convenus. Par exemple, cela comprend le degré nécessaire de précision des valeurs calculées.
- Interopérabilité : Sa capacité à interagir avec des systèmes donnés.
- Conformité réglementaire : Respect de l'application des normes, des conventions, des réglementations ou des prescriptions similaires.
- Sécurité : Aptitude à empêcher tout accès non autorisé (accidentel ou délibéré) aux programmes et données.

#### Fiabilité
- Maturité : On s'intéresse à la fréquence des défaillances dues aux défauts logiciels
- Tolérance aux fautes : Que se passe-t-il si on utilise mal le logiciel, est-ce que le logiciel peut maintenir un niveau de service en cas de mauvaise utilisation ou de violation de son interface.
- Possibilité de récupération : Capacités du logiciel à rétablir son niveau de service et à restaurer les informations directement affectées en cas de défaillance. Mesure du temps et l'effort nécessaires pour le faire.

#### Facilité d'utilisation
- ?
- ?
- Facilité de compréhension
- Facilité d'apprentissage
- Facilité d'exploitation

#### Rendement
Aptitude d'un service à inimiser l'utilisation de ses ressources pour délivrer ses prestations.

- Comportement vos-à-vis du temps
- Comportement vos-à-vis des ressources

#### Maintenabilité
- Facilité d'analyse
- Facilité de modification
- Stabilité
- Facilité de test

## La démarche qualité
- La qualité ne doit rien au hasard
- Elle ne peut reposer sur la qualité professionnelle des hommes
- Elle passe par la qualité des processus et non par la qualité des produits
- Une bonne pratique doit conduire à de bons résultats

### Les certifications
__Norme ISO 9000:2000__

La qalité doit être gérée de façon permanente.

Principe norme ISO : _"Dire ce qu'on fait, écrire ce qu'on a dit, faire et prouver ce qu'on a écrit"_

### Les 5 niveaux de qualité
- Niveau 1 : initial
    + Quelques processus définis
    + Le succès dépend des efforts d'une population de héros
    + Les plannings, les budhets,la qualité du produit ne sont généralement pas respectés
    + Succès possible, dans le stress et avec de la chance
- Niveau 2 : Reproductible
    + Suivi de coût, planification pour chaque projet
    + Estimations assez fiables
- Niveau 3 : Défini
    + Processus de réalisation institutionnalisés
    + Capitalisation systématique de l'apprentissage
    + Réutilisation du savoir-faire, du code
- Niveau 4 : Maîtrisé
    + Processus mesurés avec des métriques
    + Anticipation des risques
    + Programmes qualité
- Niveau 5 : Optimisé
    + Recherche d'amélioration

### Méthodes agiles
- Lien plus étroit entre ME-MO
- Priorité aux personnes et aux interactions sur les processus et les outils
- Des applications fonctionnelles plutôt que documentation exhaustive
- Priorité à la collaboration avec les utilisateurs plutôt qu'aux négociations contractuelles
- Acceptation des changements plutôt que planning détaillé

