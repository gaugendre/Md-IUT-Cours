# Stage
## Contraintes
Le but est de valider le DUT, de valider des compétences acquises.
Un stage de 10 semaines, on a 11 semaines pour une souplesse (fermeture pendant 1 semaine).  
Il faut que le stage soit encadré par un informaticien (personne dont le poste est constitué au moins 1/2 temps de l'informatique).  
Missions de développement (c'est large, par exemple les BD peuvent rentrer dedans. En revanche, un stage 80 réseau peut être compliqué à valider).

## Notation
Le stage donne lieu à 4 notes :

- L'évaluation par l'entreprise sur des critères déterminés par l'IUT (grille de notation).
- Note technique : Note qui repart de la première note et modulée de +/- 2 points.
- Note de soutenance (début septembre)
- Note de rapport de stage (lu par des enseignants de com)

## Encadrement
Tuteur de stage : enseignant par le DUT. Il rend visite à la 3/4ème semaine en entreprise en général pour voir si tout se passe bien.  
Ne pas hésiter à le prévenir si quelque chose se passe mal.  
Sinon si c'est vraiment grave (photocopies + café) => Xavier Merrheim.

## Documentation
Un livret de stage nous sera fourni, avec beaucoup d'infos pour nous et le maître de stage. Dans ce livret de stage en annexe on trouve les grilles de notation.

## Entreprises - Offres
M. Merrheim répercute auprès des étudiants les offres de stage des entreprises.
