# Communication - Recrutement
But de ces séances : optimiser chances de recrutement en mettant de l'informatique dans nos CV / Lettres de motivation.

## Le marché du recrutement
### Intro
Se positionner comme ayant acquis des compétences informatiques.

Premier travail : rendre le CV efficace pour une candidature dans le milieu informatique.

But final : être capable de présenter un dossier de candidature pour une simulation d'entretien.

### Marché visible / caché
Le marché de l'emploi se divise en deux entités : marché visible, et marché caché.

Les offres d'emploi peuvent apparaître de manière visible :
- sur le site de l'entreprise
- presse écrite
- à Pôle Emploi (services publics)
- ...

On a aussi des offres non diffusées :
- contrat suite à intérim
- candidature spontanée
- pré-recrutement avant la sortie de l'école
- recrutement après stage
- offres internes à l'entreprise (uniquement pour les employés de l'entreprise)
- bouche à oreille (=> candidature spontanée plutôt efficace car ciblée, pas générale)
- réseau personnel (le frère du cousin qui est informaticien, ...)
- réseaux sociaux

Le marché visible correspond à ~30% des offres et ~15% des recrutements effectifs.

70% restants pour le marché caché dans lequel on retiendra entre autres la cooptation.  
La cooptation fonctionne bien (=/= piston) : on est recommandé par un employé actuel sur la base d'informations fiables (ancien collègue de promotion par exemple, dont on sait qu'il est efficace et taillé pour ce poste).

## Organiser sa veille

L'étude du marché de l'empoi est un préalable nécessaire à toute candidature efficace. Il faut donc organiser une "veille emploi" pour exploiter toutes les voies de recrutement. __Le but étant de pouvoir trouver, au moment nécessaire, un emploi par le marché caché plus facilement que par le marché visible__.  
Dans la même idée, il est intéressant de s'ouvrir à d'autres langages (par exemple) pour rester dans les besoins du marché.

### Quelques outils
Consulter la presse spécialisée, les numéros spéciaux de certaines revues, ...

Certains sites internet :

- http://www.lesjeudis.com : Ils organisent chaque année des salons (à Lyon en novembre).
- http://www.rhonealpesjob.com : Existe pour chaque région, sorte de Pôle Emploi en ligne.
- http://www.informatique.enligne-fr.com
- http://www.aktiveo.com
- http://www.jobintree.com
- http://www.jobntic.com
- http://www.carriere-info.fr
- http://www.informatique-interim.com
- http://www.developpez.com
- http://www.passinformatique.com : Plutôt pour la veille emploi (site officiel du syntec (syndicat des métiers de l'informatique))
- http://pro.01net.com
- http://www.studyrama.com : Réservé aux étudiants, conseils sur CV, lettre de motivation.

Se rendre à des salons, forums, conférences :

- Salon de l'alternance
- Salons spécialisés dans l'informatique


