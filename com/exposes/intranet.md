# Communication interne - L'INTRANET
_La communication interne est l'ensemble des actes de communication qui se produisent à l'intérieur d'une entreprise._

- Communication verticale : entre couches hiérarchiques
- Communication horizontale : dans la même couche hiérarchique

## Introduction
Apparu en 96. Utilise internet pour partager des informations au sein d'une même structure.

## Pourquoi ?
Centraliser une multitude d'informations sur un espace unique.

Divers avantages : 

- Optimiser la circulation de l'information
- Valoriser les connaissances (capitaliser et partager l'information avec tous les membres de l'organisation)
- Simplifier l'accès et la recherche d'informations (moteur de recherche intégré)
- Faible coût de gestion

## Comment mettre en place ?
### Avant
1. Définir les besoins de l'entreprise
2. Mettre en place une équipe : équipe informatique + représentants d'autres équipes.
3. Anticiper sur la formation : Qui former, par qui, quand ?
4. Créer un modèle : tester l'outil avant de le déployer
5. Désigner les administrateurs
6. Évaluer le coût de mise en place : coûts directs et indirects (temps humain notamment)

#### Architecture à trois niveaux
- Machines clientes
- Serveur d'application
- Serveur de base de données

#### Réseau local
- Serveurs
- Logiciels

### Mise en place technique
1. Conception de la maquette
2. Conception des services
3. Test sur site
4. Déploiement : penser à mettre à disposition des ressources (userguide, forum, FAQ, ...)
5. Maintenance

## Les limites du système
- Impact sur les emplois
    + Est-ce qu'on a les compétences au sein de l'entreprise ? Faut-il embaucher ? Externaliser ?
    + Modification de certains métiers qui devront mettre à jour des données par exemple
- Accompagnement
    + Tout prévoir pour accompagner le changement et ne pas créer de rejet, de blocage.
- Contraintes budgétaires : difficile d'évaluer les coûts indirects
- Risques
    + Surcharge d'information : Trop d'information tue l'information
    + Limitation du contact direct : Les gens ne se parlent plus, moins de réunions
    + Exclusion : Concerne surtout les métiers qui n'ont pas accès facilement à ces informations (pas d'ordinateur)

## Améliorations possibles
- Ouverture sur internet pour les utilisateurs enregistrés
- Mise à disposition de questionnaires pour que les internautes puissent remplir la base de données
- Penser à se protéger des possibles agressions externes

## Conclusion
- Améliorer sa compétitivité, réactivtié, ...