# Cours à l'IUT
Ce dépôt rassemble mes notes sur les cours à l'IUT de :

- Algo avancée
- GPI
- UML
- Communication (exposés)
- Cycle de conférences

Il s'agit de cours et de notes, donc d'interprétations du cours.  
Il ne s'agit pas de cours formels ni à volonté de vérité absolue.

# Reuse
If you do reuse my work, please consider linking back to this repository 🙂