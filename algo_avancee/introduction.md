# Algo avancée - Introduction
## Présentation prof
M. KHEDDOUCI Hamamache, 2e étage  
hamamache.kheddouci@univ-lyon1.fr

## Présentation cours
On va parler des structures de données (SDD).

Deux mots-clé :  
__Structure__ & __Données__: On doit trouver une structuration des données (comment les stocker) pour le client. C'est important de structurer les données pour obtenir des algos plus efficaces. En effet, les algos sont plus efficaces s'ils fonctionnent sur des données structurées plutôt que sur des données brutes.

## Contenu
- Listes simples, bidirectionnelles, circulaires
- Piles
- Files
- Arbres
- Complexité des algorithmes
