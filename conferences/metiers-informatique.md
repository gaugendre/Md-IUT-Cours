# Les métiers de l'informatique : caractéristiques, tendances, évolution
57% des gens ne font pas le métier qu'ils envisageaient pendant leurs études.

## Secteurs clé
### Cloud computing
#### Cloud
Pourquoi ?

- Stockage des données personnelles, cloud gaming.  
- Professionnel : externalisation (applications en location), bureautique (accès par terminaux mobiles).  
- La DSI change de métier : fournisseur de technologies => fournisseur de services.

#### Avantages
- Données accessibles partout
- Plus nécessaire d'avoir de gros disques de stockages à soi
- Qualité / Sécurité des données en cas de panne

#### Inconvénients
- Dépendance du réseau
- Problème de confiance
- Sécurité professionnelle

### Big Data
Traitement rapide de (très très) gros volumes de données.

- Collecter, traiter et présenter les données rapidement.
- Les vidéos : selon CISCO ce seront les données dominantes sur le web (80%)

### Robotique
### Green IT
### Business Intelligence
Informatique décisionnelle.

_Objectifs_ : Développer des outils pour les décideurs afin de collecter, consolider, modéliser et restituer les données.

### Objets connectés
### Sécurité des SI
### Jeux (d'argent) en ligne

## Caractéristiques du secteur informatique
27% de femmes dans le secteur, 94% de CDI. 67% de cadres, 46.5k€ brut/an.

Le numérique est porteur, recrute beaucoup. C'est un secteur rentable.

Les jeunes diplômés sont des profils très recherchés, particulièrement Bac +5. Pourquoi ? Parce que connaissance des derniers logiciels et langages informatiques, plus mobiles, prêts à accepter des charges de travail plus importantes, compétences plus souples.

61% SSII, 21% édition logiciels.

Être spécialisé est un atout, les entreprises paient pour des informaticiens spécialisés.

## Entreprises qui recrutent
- SSII
    + IBM
    + CapGemini
    + ALTEN
    + CGI
    + ATOS
    + SOPRA
    + ALTRAN
    + Accenture
- Editeurs logiciels
    + Dassault system
    + SOPRA
    + CEGID)
- Constructeurs d'équipements (investissent dans le service maintant)
    + IBM
    + Microsoft
    + Apple
- Opérateurs de télécommunication
- Recherche et enseignement
    + Pas d'opposition franche entre recherche fondamentale et recherche appliquée.
    + Recherches fondamentales souvent couplées aux retombées applicatives.
    + Les enseignants du supérieur doivent être aussi des chercheurs, à cause de l'évolution permanente de la discipline.
    + Recherche appliquée : data mining, apprentissage automatique

## Parité
En France, Europe, USA, peu de femmes étudient/travaillent dans l'informatique. Alors qu'ailleurs dans le monde c'est un métier qui attire.

### Lutter contre les stéréotypes
Moins de 30% des métiers de l'informatique comportent de la programmation.

Travailler dans l'informatique c'est apprendre chaque jour, c'est aussi gérer sa carrière comme on l'entend (appartenir à une grande ou petite structure, voyager ou pas, salarié ou freelance).

## Contexte économique
Favorable. : création d'emplois, augmentation du télétravail, de plus en plus spécialisés.

### Métiers qui ont la cote

- Développement web et mobile, responsive design
- Métiers liés au big data, cloud computing et systèmes embarqués
- Product manager, pour faire le lien avec les équipes métier.
- Domaines privilégiés : web, domotique, e-commerce, robotique, jeux vidéos, ou la branche informatique elle-même (développement de nouveaux frameworks ou outils d'informaticiens).

Ces métiers sont globalement Bac +5.

### Compétences recherchées

- Maitrîse des technologies .Net ou Java (JEE) (projets + importants)
- Développement web avec PHP / HTML5-CSS3 et surtout Javascript (projets + petits, mais plus de dev)
- Les BD et SQL et les méthodes de développement continu (Git, tests, Design patterns, etc.)

### Salaires bruts annuels
Voir diapo

- Webmaster
- Admin sys
- Admin BD
- Chef de projet MOA
- Responsable BI (Polytech Lyon)